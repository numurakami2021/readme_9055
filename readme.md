# README
Readmeを書いてみよう。

# 見出しの大きさ
## 2
\#が2つ
### 3
\#が3つ
###### 6
\#が6つ

# 装飾
### イタリック体
sekine hiroki  
*sekine hiroki*  
_sekine hiroki_

### 太字
関根 大輝  
**関根 大輝**  
__関根 大輝__

### 取り消し線
~~取り消し線~~

# リンク
ここで[moodle日大](https://moodle.ce.cst.nihon-u.ac.jp/)に飛べます。

# テーブル
|日付|時間|食べたもの|
|------|------|------|
|2/10|昼|すき焼き|
|2/10|夜|マック|